import torch
import random
import numpy as np
from helper import plot
from collections import deque
from model import Linear_QNet, QTrainer
from snake import SnakeGameAI, Direction, Point

MAX_MEMORY = 300_000
BATCH_SIZE = 3_000
LR = 0.001


class Agent:
    def __init__(self):
        self.number_games = 0
        self.epsilon = 0  # Control de randomness
        self.gamma = 0.95  # Discount factor
        self.memory = deque(maxlen=MAX_MEMORY)  # popleft()
        self.model = Linear_QNet(11, 256, 3)
        self.trainer = QTrainer(self.model, lr=LR, gamma=self.gamma)

    def get_state(self, snake):
        head = snake.snake[0]
        point_l = Point(head.x - 20, head.y)
        point_r = Point(head.x + 20, head.y)
        point_u = Point(head.x, head.y - 20)
        point_d = Point(head.x, head.y + 20)

        dir_l = snake.direction == Direction.LEFT
        dir_r = snake.direction == Direction.RIGHT
        dir_u = snake.direction == Direction.UP
        dir_d = snake.direction == Direction.DOWN

        state = [
            # Danger straight
            (dir_r and snake.is_collision(point_r)) or
            (dir_l and snake.is_collision(point_l)) or
            (dir_u and snake.is_collision(point_u)) or
            (dir_d and snake.is_collision(point_d)),

            # Danger right
            (dir_u and snake.is_collision(point_r)) or
            (dir_d and snake.is_collision(point_l)) or
            (dir_l and snake.is_collision(point_u)) or
            (dir_r and snake.is_collision(point_d)),

            # Danger left
            (dir_d and snake.is_collision(point_r)) or
            (dir_u and snake.is_collision(point_l)) or
            (dir_r and snake.is_collision(point_u)) or
            (dir_l and snake.is_collision(point_d)),

            # Move direction
            dir_l,
            dir_r,
            dir_u,
            dir_d,

            # Food location
            snake.food.x < snake.head.x,  # Food to the left
            snake.food.x > snake.head.x,  # Food to the right
            snake.food.y < snake.head.y,  # Food to the top
            snake.food.y > snake.head.y,  # Food to the bottom
        ]

        return np.array(state, dtype=int)

    def remember(self, state, action, reward, next_state, done):
        # popleft if memory is full
        self.memory.append((state, action, reward, next_state, done))

    def train_long_memory(self) -> None:
        if len(self.memory) > BATCH_SIZE:
            mini_sample = random.sample(self.memory, BATCH_SIZE)

        else:
            mini_sample = self.memory

        states, actions, rewards, next_states, dones = zip(*mini_sample)
        self.trainer.train_step(states, actions, rewards, next_states, dones)

    def train_short_memory(self, state, action, reward, next_state, done):
        self.trainer.train_step(state, action, reward, next_state, done)

    def get_action(self, state):
        # Random actions: tradeoff exploration/exploitation
        self.epsilon = 80 - self.number_games
        final_move = [0, 0, 0]

        if random.randint(0, 200) < self.epsilon:
            move = random.randint(0, 2)
            final_move[move] = 1

        else:
            state0 = torch.tensor(state, dtype=torch.float)
            prediction = self.model(state0)
            move = torch.argmax(prediction).item()
            final_move[move] = 1

        return final_move


def train():
    try:
        plot_scores = []
        plot_mean_scores = []
        total_score = 0
        record = 0
        agent = Agent()
        snake = SnakeGameAI()

        while True:
            # Get old state
            state_old = agent.get_state(snake)

            # Get action
            final_move = agent.get_action(state_old)

            # Perform action and get new state
            reward, done, score = snake.play_step(final_move)
            new_state = agent.get_state(snake)

            # Train short memory
            agent.train_short_memory(
                state_old, final_move, reward, new_state, done)

            # Remember
            agent.remember(state_old, final_move, reward, new_state, done)

            if done:
                # Train long memory, plot scores
                snake.reset()
                agent.number_games += 1
                agent.train_long_memory()

                if score > record:
                    record = score

                    agent.model.save()

                print(
                    f'Game: {agent.number_games} Score: {score} Record: {record}')

                # Plot scores
                plot_scores.append(score)
                total_score += score
                mean_score = total_score / agent.number_games
                plot_mean_scores.append(mean_score)
                plot(plot_scores, plot_mean_scores)

    except Exception as e:
        print(e)


if __name__ == "__main__":
    train()
